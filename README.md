# Towards Robust Reinforcement Learning

This repository contains the resources used for the project, `Towards Robust Reinforcement Learning`, including a sample notebook.
Note that some of the evaluations and attacks are handcrafted in notebooks that are difficult to share so a sample notebook is provided for reference. To run the notebook examples, start with base.ipynb. Make sure to include atari_wrapper and unzip jsAnimation.zip provided in the /notebooks directory first to visualize the observed frames.

### Configuration
The code was run on Jupyter Notebooks on GCP with the following specifications:
- Image: PyTorch/fastai/CUDA10.0
- GPU/CPU/RAM: 1 x NVIDIA Tesla P4 / 4 x CPUs / 32GB
- Torch: '1.0.1.post2'