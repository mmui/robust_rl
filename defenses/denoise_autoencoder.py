import torch
import torch.nn as nn

class DenoisingAutoencoder(nn.Module):
    
    def __init__(self):
    
        super(DenoisingAutoencoder, self).__init__()

        self.conv1e = nn.Conv2d(1, 24, 3, padding=2)
        self.norm1e = nn.BatchNorm2d(24)
        self.conv2e = nn.Conv2d(24, 48, 3, padding=2)      
        self.norm2e = nn.BatchNorm2d(48)
        self.conv3e = nn.Conv2d(48, 96, 3, padding=2)
        self.norm3e = nn.BatchNorm2d(96)
        self.conv4e = nn.Conv2d(96, 128, 3, padding=2)
        self.norm4e = nn.BatchNorm2d(128)
        self.conv5e = nn.Conv2d(128, 256, 3, padding=2)
        self.norm5e = nn.BatchNorm2d(256)
        self.mp1e   = nn.MaxPool2d(2, return_indices=True) 

        self.mp1d = nn.MaxUnpool2d(2)
        self.conv5d = nn.ConvTranspose2d(256, 128, 3, padding=2)
        self.norm5d = nn.BatchNorm2d(128)
        self.conv4d = nn.ConvTranspose2d(128, 96, 3, padding=2)
        self.norm4d = nn.BatchNorm2d(96)
        self.conv3d = nn.ConvTranspose2d(96, 48, 3, padding=2)
        self.norm3d = nn.BatchNorm2d(48)
        self.conv2d = nn.ConvTranspose2d(48, 24, 3, padding=2)
        self.norm2d = nn.BatchNorm2d(24)
        self.conv1d = nn.ConvTranspose2d(24, 1, 3, padding=2)
        self.norm1d = nn.BatchNorm2d(1)
        
    
    def forward(self, x):
        # Encoder
        x = self.conv1e(x)
        x = F.relu(x)
        x = self.conv2e(x)
        x = F.relu(x)
        x = self.conv3e(x)
        x = F.relu(x)
        x = self.conv4e(x)
        x = F.relu(x)
        x = self.conv5e(x)
        x = F.relu(x)
        x, i = self.mp1e(x)
        
         # Decoder
        x = self.mp1d(x, i)
        x = self.conv5d(x)
        x = F.relu(x)
        x = self.conv4d(x)
        x = F.relu(x)
        x = self.conv3d(x)
        x = F.relu(x)
        x = self.conv2d(x)
        x = F.relu(x)
        x = self.conv1d(x)
        x = F.relu(x)
        
        return x


class Flatten(nn.Module):
    def forward(self, input):
        return input.view(input.size(0), -1)
    
class Unflatten(nn.Module):
    def forward(self, input, size=1024):
        return input.view(input.size(0), size, 1, 1)

class VAE(nn.Module):
    def __init__(self, image_channels=3, h_dim=1024, z_dim=32):
        super(VAE, self).__init__()
        self.encoder = nn.Sequential(
            nn.Conv2d(image_channels, 24, kernel_size=3, padding=2),
            nn.BatchNorm2d(24),
            nn.ReLU(),
            nn.Conv2d(24, 48, kernel_size=3, padding=2),
            nn.BatchNorm2d(48),
            nn.ReLU(),
            nn.Conv2d(48, 128, kernel_size=3, padding=2),
            nn.ReLU(),
            nn.Conv2d(96, 128, kernel_size=4, stride=2),
            nn.ReLU(),
            Flatten()
        )
       
        #128 x 38 x 38
        self.fc1 = nn.Linear(1036800, z_dim)
        self.fc2 = nn.Linear(1036800, z_dim)
        self.fc3 = nn.Linear(z_dim, h_dim)
        
        self.decoder = nn.Sequential(
            Unflatten(),
            nn.ConvTranspose2d(h_dim, 64, kernel_size=7, stride=1), #2
            nn.BatchNorm2d(64),
            nn.ReLU(),
            nn.ConvTranspose2d(64, 32, kernel_size=3, stride=3), #4
            nn.BatchNorm2d(32),
            nn.ReLU(),
            nn.ConvTranspose2d(32, image_channels, kernel_size=4, stride=4), #8
            nn.ReLU(),
            nn.ConvTranspose2d(32, 16, kernel_size=2, stride=2), #16
            nn.ReLU(),
            nn.ConvTranspose2d(16, image_channels, kernel_size=2, stride=2), #32
            nn.Sigmoid(),
        )
        
    def reparameterize(self, mu, logvar):
        std = logvar.mul(0.5).exp_()
        esp = torch.randn(*mu.size()).cuda()
        z = mu + std * esp
        return z
    
    def bottleneck(self, h):
        mu, logvar = self.fc1(h), self.fc2(h)
        z = self.reparameterize(mu, logvar)
        return z, mu, logvar

    def encode(self, x):
        h = self.encoder(x)
        z, mu, logvar = self.bottleneck(h)
        return z, mu, logvar

    def decode(self, z):
        z = self.fc3(z)
        z = self.decoder(z)
        return z

    def forward(self, x):
        z, mu, logvar = self.encode(x)
        z = self.decode(z)
        return z, mu, logvar

def get_loss(reconstructed, input, mu, logvar):
#     BCE = F.binary_cross_entropy(reconstructed, input, size_average=False)
    BCE = F.mse_loss(reconstructed, input, size_average=False)
    KLD = -0.5 * torch.mean(1 + logvar - mu.pow(2) - logvar.exp())

    return BCE + KLD, BCE, KLD