import torch
import torch.nn as nn

import random

class DQN(nn.Module):
    def __init__(self, state_shape, action_size):
        super(DQN, self).__init__()
        
        self.state_shape = state_shape
        self.action_size = action_size
        
        self.cnn = nn.Sequential(
            nn.Conv2d(self.state_shape[-1], 32, kernel_size=8, stride=4),
            nn.ReLU(),
            nn.Conv2d(32, 64, kernel_size=4, stride=2),
            nn.ReLU(),
            nn.Conv2d(64, 64, kernel_size=3, stride=1),
            nn.ReLU()
        )
        self.fc = nn.Sequential(
            nn.Linear(7 * 7 * 64, 512),
            nn.ReLU(),
            nn.Linear(512, self.action_size)
        )

    def forward(self, x):
        x = self.cnn(x)
        x = x.view(x.size(0), -1)

        # run through the FC layer get the Q-Value
        return self.fc(x)

    # TODO: extract action out of DQN into agent class
    def action(self, state, pr):
        # agent selects mostly random actions at the beginning due to pr curve
        if random.random() > pr:
            q_value = self.forward(state)
            action  = q_value.max(1)[1].data[0]
        else:
            action = torch.tensor(random.randrange(self.action_size)).cuda()
        return action
