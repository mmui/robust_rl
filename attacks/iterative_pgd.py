import torch
import torch.nn.functional as F
from torch.autograd import Variable

class IterativePGDAttack(object):
    def __init__(self, model, is_cuda=True, eps=0.01, clip_min=0, clip_max=255, step_size=0.01, env=None):
        self.model = model
        self.is_cuda = is_cuda
        self.eps = eps
        self.clip_min = clip_min
        self.clip_max = clip_max
        self.step_size = step_size
        self.env = env

    def step(self, state):
        if CUDA:
            state = state.cuda()
        
        state = state.view(1, 1, 84, 84)
        q_vals = self.model(state)
        action = self.model.action(state, 0)
        
        next_state, reward, done, info = self.env.step(action)
        next_observed_state = Variable(torch.FloatTensor(np.float32(next_state)).unsqueeze(0))
        if CUDA:
            next_observed_state = next_observed_state.cuda()
        next_observed_state = next_observed_state.view(1, 1, 84, 84)
        return q_vals, self.model(next_observed_state), next_state 


    def get_adversarial(self, image, label):
        observed = Variable(torch.FloatTensor(np.float32(image)).unsqueeze(0), requires_grad=True)
        for i in range(0, self.step_size):

            q_vals, target_q_vals, next_state = self.step_model(observed)
            loss = F.kl_div(F.softmax(q_vals), Variable(F.softmax(target_q_vals), requires_grad=False))
            self.model.zero_grad()
            loss.backward(retain_graph=True)
            
            next_observed = Variable(torch.FloatTensor(np.float32(next_state)).unsqueeze(0))
            if observed.requires_grad:
                grad_observed = torch.autograd.grad(outputs=loss, inputs=observed)[0]
            
            gradient_adv = grad_observed.sign() * step_size
            next_observed_adv = next_observed - gradient_adv

            # clipping
            next_observed_adv = torch.max(
                torch.min(next_observed_adv, next_observed + eps), 
                next_observed - eps)

            observed = next_observed_adv
        
        return torch.clamp(observed, clip_min, clip_max)