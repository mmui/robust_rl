import torch
import torch.nn.functional as F
from torch.autograd import Variable

class FastGradientSignMethodAttack(object):
    def __init__(self, model, is_cuda=True, eps=0.5):
        self.model = model
        self.is_cuda = is_cuda
        self.eps = eps

    def get_adversarial(self, image, label):
        q_vals = self.model(image)
        target_q_vals = self.model(label)
        loss = F.kl_div(F.softmax(q_vals), Variable(F.softmax(target_q_vals), requires_grad=False))
        self.model.zero_grad()
        
        # backprop
        loss.backward(retain_graph=True)
        sign = torch.autograd.grad(outputs=loss, inputs=image)[0].sign()
        adv_noise = -self.eps * sign
        adv_noise = adv_noise.view(1, 84, 84, 1)
        return adv_noise + image
