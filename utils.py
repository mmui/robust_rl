from JSAnimation.IPython_display import display_animation
from matplotlib import animation
import matplotlib.pyplot as plt

from IPython.display import display as py_display
from IPython.display import clear_output


def display_frames(frames):
    """
    displays a list of frames as a gif, with controls
    """
    patch = plt.imshow(frames[0])
    plt.axis('off')

    def animate(i):
        patch.set_data(frames[i])

    gif = animation.FuncAnimation(plt.gcf(), animate, frames=len(frames), interval=50)
    py_display(display_animation(gif, default_mode='loop'))
